//
//  VideoViewCell.swift
//  Tick-tock-list
//
//  Created by Kuts, Andrey on 12/25/19.
//  Copyright © 2019 Kuts, Andrey. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Kingfisher
import AVFoundation

protocol VideoViewCellProtocol {
    func setupUI()
    func configure(withModel model: VideoCellModel)
    func playVideo()
    func stopPlayingVideo()
}

class VideoViewCell: UICollectionViewCell, Reusable, VideoViewCellProtocol {

    private var playButton = UIButton()
    private var profileButton = ViewBuilder.makeButtonWith(image: UIImage(named: "profile"))
    private var likeButton = ViewBuilder.makeButtonWith(image: UIImage(named: "like"))
    private var messageButton = ViewBuilder.makeButtonWith(image: UIImage(named: "message"))
    private var titleLabel = ViewBuilder.makeLabelWithShadow()
    private var infoLabel = ViewBuilder.makeLabelWithShadow(font: UIFont.boldSystemFont(ofSize: Constants.Fonts.size))
    private var previewImageView = ViewBuilder.makeImageView()
    private var model: VideoCellModelProtocol?
    private var videoView = ViewBuilder.makeVideoView()
    private var disposeBug = DisposeBag()

    override func prepareForReuse() {
        super.prepareForReuse()
        
    }

    func configure(withModel model: VideoCellModel) {

        self.model = model
        self.titleLabel.text = model.title
        self.infoLabel.text = model.info

        if let url = model.previewImageURL {
            previewImageView.kf.setImage(with: url)
        } else {
            previewImageView.image = nil
        }

        if let player = model.videoPlyaer.value {
            DispatchQueue.main.async {
                player.seek(to: .zero)
                let playerLayer = AVPlayerLayer(player: player)
                self.videoView.setupMovieView(withPlayerLayer: playerLayer)
                self.videoView.setupPlayLayer()
                self.setupPlayButton()
            }
        } else {
            configureRx()
        }
    }

    func configureRx() {
        model?.videoPlyaer
            .subscribe(onNext: { [weak self] player in
                player?.seek(to: .zero)
                let playerLayer = AVPlayerLayer(player: player)
                self?.videoView.setupMovieView(withPlayerLayer: playerLayer)
                self?.videoView.setupPlayLayer()
                self?.setupPlayButton()
            })
            .disposed(by: disposeBug)
    }

    func setupUI() {
        setupPreviewImageView()
        setupTitleLabel()
        setupInfoLabel()
        setProfileButton()
        setMessageButton()
        setLikeButton()
        setupVideoView()
    }

    func playVideo() {
        model?.playbackManager.onPlayButtonPressed(isPlay: true)
    }

    func stopPlayingVideo() {
        model?.playbackManager.onPlayButtonPressed(isPlay: false)
    }
}

private extension VideoViewCell {

    func setupTitleLabel() {
        titleLabel.removeFromSuperview()
        self.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: Constants.Inset.bottom).isActive = true
        titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        titleLabel.widthAnchor.constraint(equalToConstant: frame.size.width - 24).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: Constants.Size.Label.height).isActive = true
    }

    func setupPreviewImageView() {
        previewImageView.removeFromSuperview()
        self.addSubview(previewImageView)
        previewImageView.translatesAutoresizingMaskIntoConstraints = false
        previewImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        previewImageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        previewImageView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        previewImageView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    }

    func setupVideoView() {
        DispatchQueue.main.async {
            self.videoView = ViewBuilder.makeVideoView()
            self.addSubview(self.videoView)
        }
    }

    func setupInfoLabel() {
        infoLabel.removeFromSuperview()
        self.addSubview(infoLabel)
        infoLabel.translatesAutoresizingMaskIntoConstraints = false
        infoLabel.bottomAnchor.constraint(equalTo: titleLabel.topAnchor).isActive = true
        infoLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        infoLabel.widthAnchor.constraint(equalToConstant: frame.size.width - 24).isActive = true
        infoLabel.heightAnchor.constraint(equalToConstant: 22).isActive = true
    }

    func setupPlayButton() {
        playButton.removeFromSuperview()
        self.addSubview(playButton)
        playButton.translatesAutoresizingMaskIntoConstraints = false
        playButton.topAnchor.constraint(equalTo: topAnchor, constant: Constants.Inset.PlayButton.vertical).isActive = true
        playButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Constants.Inset.PlayButton.vertical).isActive = true
        playButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.Inset.PlayButton.horizotal).isActive = true
        playButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.Inset.PlayButton.horizotal).isActive = true
        playButton.setImage(UIImage(named: "play"), for: .selected)
        playButton.addTarget(self, action: #selector(onPlayButtonPressed), for: .touchUpInside)
        playButton.isSelected = false
    }

    @objc func onPlayButtonPressed(_ sender: UIButton) {
        model?.playbackManager.onPlayButtonPressed(isPlay: sender.isSelected)
        sender.isSelected.toggle()
    }

    func setProfileButton() {
        profileButton.removeFromSuperview()
        self.addSubview(profileButton)
        profileButton.translatesAutoresizingMaskIntoConstraints = false
        profileButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: Constants.Inset.bottom).isActive = true
        profileButton.rightAnchor.constraint(equalTo: rightAnchor, constant: Constants.Inset.right).isActive = true
        profileButton.widthAnchor.constraint(equalToConstant: Constants.Size.Button.defaultWidth).isActive = true
        profileButton.heightAnchor.constraint(equalToConstant: Constants.Size.Button.defaultHeight).isActive = true
    }

    func setLikeButton() {
        likeButton.removeFromSuperview()
        self.addSubview(likeButton)
        likeButton.translatesAutoresizingMaskIntoConstraints = false
        likeButton.bottomAnchor.constraint(equalTo: messageButton.topAnchor, constant: Constants.Inset.between).isActive = true
        likeButton.rightAnchor.constraint(equalTo: rightAnchor, constant: Constants.Inset.right).isActive = true
        likeButton.widthAnchor.constraint(equalToConstant: Constants.Size.Button.defaultWidth).isActive = true
        likeButton.heightAnchor.constraint(equalToConstant: Constants.Size.Button.defaultHeight).isActive = true
    }

    func setMessageButton() {
        messageButton.removeFromSuperview()
        self.addSubview(messageButton)
        messageButton.translatesAutoresizingMaskIntoConstraints = false
        messageButton.bottomAnchor.constraint(equalTo: profileButton.topAnchor, constant: Constants.Inset.between).isActive = true
        messageButton.rightAnchor.constraint(equalTo: rightAnchor, constant: Constants.Inset.right).isActive = true
        messageButton.widthAnchor.constraint(equalToConstant: Constants.Size.Button.defaultWidth).isActive = true
        messageButton.heightAnchor.constraint(equalToConstant: Constants.Size.Button.defaultHeight).isActive = true
    }
}
