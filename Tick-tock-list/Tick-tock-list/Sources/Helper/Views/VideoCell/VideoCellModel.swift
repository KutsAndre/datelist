//
//  VideoCellModel.swift
//  Tick-tock-list
//
//  Created by Kuts, Andrey on 12/25/19.
//  Copyright © 2019 Kuts, Andrey. All rights reserved.
//

import Foundation.NSURL
import AVFoundation.AVPlayerItem
import RxSwift
import RxCocoa

enum VideoModelState {
    case notReady
    case loading
    case ready
    case plaing
    case pause
    case finished
    case failed
}

protocol VideoCellModelProtocol: class {
    var state: BehaviorRelay<VideoModelState> { get }
    var title: String { get }
    var info: String { get }
    var previewImageURL: URL? { get }
    var videoPlyaer: BehaviorRelay<AVPlayer?> { get }
    var playbackManager: PlaybackManagerProtocol { get }

    func playVideo()
    func stopVideo()
}

class VideoCellModel: VideoCellModelProtocol {

    var videoPlyaer: BehaviorRelay<AVPlayer?>
    let playbackManager: PlaybackManagerProtocol
    let loadedPlayerItem: BehaviorRelay<AVPlayerItem?>
    let state: BehaviorRelay<VideoModelState>

    let title: String
    let info: String
    var previewImageURL: URL?

    private let user: UserModel
    private let videoQuality: VideoQuality
    private let videoLoader = VideoPreloader()
    private let disposeBag = DisposeBag()

    init(userModel: UserModel) {
        self.state = .init(value: .notReady)
        self.loadedPlayerItem = .init(value: nil)
        self.videoPlyaer = .init(value: nil)
        self.videoQuality = .low
        self.user = userModel
        self.title = user.name
        self.info = "\(user.age)"
        self.previewImageURL = user.previewPhotoURL
        self.playbackManager = PlaybackManager()
        self.playbackManager.delegate = self

        configureRx()
        preloadVideo()
    }

    func playVideo() {
        playbackManager.playVideo()
    }

    func stopVideo() {

    }
}

// MARK: - PlaybackManagerDelegate

extension VideoCellModel: PlaybackManagerDelegate {

    func playbackManager(_ streamPlaybackService: PlaybackManagerProtocol, playerReadyToPlay player: AVPlayer) {
        videoPlyaer.accept(player)
        state.accept(.ready)
    }

    func playbackManager(_ streamPlaybackService: PlaybackManagerProtocol, playerStartPlay player: AVPlayer) {
        state.accept(.plaing)
    }

    func playbackManager(_ streamPlaybackService: PlaybackManagerProtocol, playFinished: Bool) {
        state.accept(.finished)
    }

    func playbackManager(_ streamPlaybackService: PlaybackManagerProtocol, playerError error: Error?) {
        state.accept(.failed)
    }
}

private extension VideoCellModel {

    var videoUrl: URL? {
        switch videoQuality {
        case .high: return URL(string: user.cards.first { $0.video != nil }?.video?.files.hls ?? "")
        case .low: return URL(string: user.cards.first { $0.video != nil }?.video?.files.hls ?? "")
        }
    }

    func configureRx() {
        loadedPlayerItem
            .filter { $0 != nil }.map { $0! }
            .subscribe(onNext: { [weak self] item in
                self?.state.accept(.ready)
                self?.playbackManager.setPlayerItem(item)
            })
            .disposed(by: disposeBag)
    }

    func preloadVideo() {
        switch state.value {
        case .notReady, .failed, .finished:
            if let url = videoUrl {
                state.accept(.loading)
                videoLoader.loadUrl(videoURL: url, playerItemObserver: loadedPlayerItem)
            } else {
                state.accept(.failed)
            }
        case .plaing, .loading, .pause, .ready:
            break
        }
    }
}
