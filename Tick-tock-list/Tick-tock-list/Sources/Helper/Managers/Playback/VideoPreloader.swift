//
//  VideoPreloader.swift
//  Tick-tock-list
//
//  Created by Kuts, Andrey on 1/2/20.
//  Copyright © 2020 Kuts, Andrey. All rights reserved.
//

import AVFoundation
import RxCocoa

struct VideoPreloader {

    func loadUrl(videoURL: URL, playerItemObserver: BehaviorRelay<AVPlayerItem?>) {
        let asset = AVAsset(url: videoURL)
        _ = asset.observe(\AVURLAsset.isPlayable, options: [.new, .initial]) { urlAsset, _ in
            guard urlAsset.isPlayable else {
                playerItemObserver.accept(nil)
                return
            }
            asset.loadValuesAsynchronously(forKeys: Constants.assetKeys) {
                let playerItem = AVPlayerItem(asset: asset, automaticallyLoadedAssetKeys: Constants.assetKeys)
                playerItemObserver.accept(playerItem)
            }
        }
    }
}
