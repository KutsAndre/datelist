//
//  VideoQuality.swift
//  Tick-tock-list
//
//  Created by Kuts, Andrey on 12/28/19.
//  Copyright © 2019 Kuts, Andrey. All rights reserved.
//

enum VideoQuality: String {
//    case auto
    case high
    case low
}
