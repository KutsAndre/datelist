//
//  MainViewModel.swift
//  Tick-tock-list
//
//  Created by Kuts, Andrey on 12/24/19.
//  Copyright © 2019 Kuts, Andrey. All rights reserved.
//

import AVFoundation
import RxSwift
import RxDataSources

class MainSceneViewModel {

    private let disposeBag: DisposeBag
    private var model: MainVMModel

    var playerManager: PlaybackManagerProtocol
    var sections: PublishSubject<[SectionModel<VideoCellModel>]>
    var isDataRedy: BehaviorSubject<Bool>

    init(model: MainVMModel = MainVMModel(),
         disposeBag: DisposeBag = DisposeBag(),
         playerManager: PlaybackManagerProtocol = PlaybackManager()) {

        self.model = model
        self.disposeBag = disposeBag
        self.sections = .init()
        self.isDataRedy = .init(value: false)
        self.playerManager = playerManager

        configureRx()
    }

    func loadData() {
        isDataRedy.onNext(false)
        model.loadUserList()
    }

    private func configureRx() {
        model.userListDriver
            .skip(1)
            .map { userList -> SectionModel<VideoCellModel> in
                var items: [VideoCellModel] = []
                userList.users[0...10].forEach { userModel in items.append(VideoCellModel(userModel: userModel)) }
                return SectionModel(items: items)
            }
            .drive(onNext: { [weak self] sectionModel in
                self?.isDataRedy.onNext(true)
                self?.sections.onNext([sectionModel])
            })
            .disposed(by: disposeBag)
    }
}
