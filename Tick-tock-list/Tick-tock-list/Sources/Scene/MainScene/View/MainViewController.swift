//
//  MainViewController.swift
//  Tick-tock-list
//
//  Created by Kuts, Andrey on 12/24/19.
//  Copyright © 2019 Kuts, Andrey. All rights reserved.
//

import UIKit
import Differentiator
import RxDataSources
import RxSwift

class MainViewController: UIViewController {

    private lazy var collectionView = ViewBuilder.makeCollectionView()
    private lazy var dataSource = makeDataSource()
    private var disposeBug = DisposeBag()
    private weak var lastPlaingView: VideoViewCell?

    var viewModel: MainSceneViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        configureRx()
        viewModel.loadData()

        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            self?.setPlayingVideoOnCurrentCell()
        }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override var prefersStatusBarHidden: Bool {
        return false
    }
}

private extension MainViewController {

    func setupUI() {
        setupCollectionView()
    }

    func configureRx() {

        viewModel.sections
            .bind(to: collectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBug)

        (collectionView as UIScrollView).rx
            .didEndDecelerating
            .delaySubscription( .seconds(1), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.setPlayingVideoOnCurrentCell()
            }).disposed(by: disposeBug)
    }

    func makeDataSource() -> RxCollectionViewSectionedReloadDataSource<SectionModel<VideoCellModel>> {
        return RxCollectionViewSectionedReloadDataSource<SectionModel>(
            configureCell: { dataSource, collectionView, index, element -> UICollectionViewCell in
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VideoViewCell.reuseIdentifier, for: index)
                if let videoCell = cell as? VideoViewCell {
                    videoCell.setupUI()
                    videoCell.configure(withModel: element)
                }
                return cell
        })
    }

    func setupCollectionView() {
        collectionView.register(VideoViewCell.self, forCellWithReuseIdentifier: VideoViewCell.reuseIdentifier)
        self.view.addSubview(collectionView)
        collectionView.isPagingEnabled = true
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.pinEdges(to: self.view)
    }

    func setPlayingVideoOnCurrentCell() {
        let visibleRect = CGRect(origin: self.collectionView.contentOffset, size: self.collectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let indexPath = self.collectionView.indexPathForItem(at: visiblePoint),
            let cell = collectionView.cellForItem(at: indexPath) as? VideoViewCell else {
                return
        }

        collectionView.visibleCells.forEach {
            ($0 as? VideoViewCell)?.stopPlayingVideo()
        }
        cell.playVideo()
    }
}
