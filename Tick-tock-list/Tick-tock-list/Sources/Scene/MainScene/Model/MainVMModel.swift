//
//  MainVMModel.swift
//  Tick-tock-list
//
//  Created by Kuts, Andrey on 12/26/19.
//  Copyright © 2019 Kuts, Andrey. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class MainVMModel {

    internal lazy var userListDriver = userList.asDriver(onErrorDriveWith: .never())
    internal lazy var videoQualityManager = VideoSourceSelectorService()
    internal lazy var videoQuality = BehaviorRelay<VideoQuality>(value: .high)

    private let userList: BehaviorSubject<UserListModel>
    private let userApi: MainUserNetworking

    // WARNING: TMP
    private let bundl = Bundle.main
    private let fileName = "json"

    init() {
        userList = BehaviorSubject<UserListModel>(value: UserListModel(users: []))
        userApi = ServiceLocator.inject()

        checkSpeed()
    }

    func loadUserList() {
        guard let path = bundl.path(forResource: fileName, ofType: "json"),
            let url = URL(fileURLWithPath: path) as? URL,
            let data = try? Data(contentsOf: url),
            let decodedList = try? Networking.decoder.decode(UserListModel.self, from: data) else {
                return
        }
        self.userList.onNext(decodedList)
    }

    private func checkSpeed() {
        videoQualityManager.makeRequestVideoQuality()
        videoQualityManager.onVideoQualityReceivedCallback = { [weak self] quality in
            self?.videoQuality.accept(quality)
        }
    }
}
